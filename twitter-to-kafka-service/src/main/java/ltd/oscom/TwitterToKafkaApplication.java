package ltd.oscom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;
import ltd.oscom.config.TwitterKeyWordsConfiguration;
import ltd.oscom.runner.StreamRunner;

@SpringBootApplication
public class TwitterToKafkaApplication implements CommandLineRunner {
	
	private final TwitterKeyWordsConfiguration twitterKeyWordsConfiguration;
	private final StreamRunner streamRunner;
	
	public TwitterToKafkaApplication(TwitterKeyWordsConfiguration twitterKeyWordsConfiguration,StreamRunner streamRunner) {
		this.twitterKeyWordsConfiguration = twitterKeyWordsConfiguration;
		this.streamRunner = streamRunner;
	}
	
	private static final Logger LOG = LoggerFactory.getLogger(TwitterToKafkaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(TwitterToKafkaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		LOG.debug("---- Inside the initalization block ----");
		streamRunner.start();
		
	}

}
