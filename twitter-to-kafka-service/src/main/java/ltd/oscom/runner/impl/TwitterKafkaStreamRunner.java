package ltd.oscom.runner.impl;

import java.util.Arrays;
import java.util.stream.Stream;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import ltd.oscom.config.TwitterKeyWordsConfiguration;
import ltd.oscom.listner.TwitterKafkaListener;
import ltd.oscom.runner.StreamRunner;
import twitter4j.FilterQuery;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

@Slf4j
@Component
public class TwitterKafkaStreamRunner implements StreamRunner{
	
	private final  TwitterKeyWordsConfiguration twitterKeyWordsConfiguration;
	private final TwitterKafkaListener twitterKafkaListener;
	private  TwitterStream twitterStream;
	
	
	public TwitterKafkaStreamRunner(TwitterKeyWordsConfiguration twitterKeyWordsConfiguration,TwitterKafkaListener twitterKafkaListener) {
		this.twitterKeyWordsConfiguration = twitterKeyWordsConfiguration;
		this.twitterKafkaListener = twitterKafkaListener;
	}
	

	@Override
	public void start() throws TwitterException {
		twitterStream = new TwitterStreamFactory().getInstance();
		twitterStream.addListener(twitterKafkaListener);
		addFilter();
	}
	
	@PreDestroy
	public void shutdown() {
		if(twitterStream != null)
			twitterStream.shutdown();
		log.info("Closed the twitter stream.");
			
	}


	private void addFilter() {
		String keywords[] = twitterKeyWordsConfiguration.getTwitterKeyWords().toArray(new String[0]);
		FilterQuery filterQuery = new FilterQuery(keywords);
		log.info("Added filter in the twitter stream {}",Arrays.toString(keywords));
		twitterStream.filter(filterQuery);
	}

}
